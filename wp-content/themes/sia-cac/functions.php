<?php

	function theme_scripts() {

		wp_enqueue_style( 'styles', get_template_directory_uri() . '/assets/css/style.css' );

		wp_deregister_script( 'jquery' );
		wp_register_script( 'jquery', get_template_directory_uri() . '/assets/js/vendor.js', false, null, true );
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'main', get_template_directory_uri() . '/assets/js/main.js', ['jquery'], null, true );
	}

	function theme_main_setup() {

		register_nav_menus( array(
			'header_menu' => 'Menu in burger',
		));
	}

	if( function_exists('acf_add_options_page') ) {
		
		acf_add_options_page(array(
			'page_title' 	=> 'Theme Options',
			'menu_title'	=> 'Theme Options',
			'menu_slug' 	=> 'theme-general-settings',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));	
	}


	/* Remove br from CONTACT FORM 7
	=======================================*/
	add_filter('wpcf7_autop_or_not', '__return_false');



	
	/* Init Custom Post type
	=======================================*/

	include (get_template_directory() . '/assets/inc/custom-post-type-services.php');

	



	add_action('wp_enqueue_scripts', 'theme_scripts');
	add_action('after_setup_theme', 'theme_main_setup');



