$(function () {

	if($('.reviews__slider').length > 0) {
	    $('.reviews__slider').slick({
	        slidesToShow: 1,
	        infinite: true,
	        dots: false,
	        arrows: false,
	        speed: 500,
	        fade: true,
	        cssEase: 'linear',
	        slidesToScroll: 1,
	    });
	}

	$(".reviews__slider-btn--prev").on("click", function() {
	    $(".reviews__slider").slick("slickPrev");
	});

	$(".reviews__slider-btn--next").on("click", function() {
	    $(".reviews__slider").slick("slickNext");
	});  



	$(".tabs-js").on("click", "h2:not(.active)", function(e) {
	        $(this)
	        .addClass("active")
	        .siblings()
	        .removeClass("active")
	        .parents(".tabs-info-js")
	        .find(".contact-info__content")
	        .children()
	        .hide()
	        .eq($(this).index())
	        .fadeIn(300);

	});



	$(".scrollto").click(function () {
	    var elementClick = $(this).attr("href"),
	        destination = $(elementClick).offset().top;
	    $("html:not(:animated),body:not(:animated)").animate({scrollTop: destination}, 1100);
	    return false;
	});


	wow = new WOW(
        {
          mobile:       false,    
        }
      )
    wow.init();



	$('.burger-content').on('click', function () {
		  $('.header__menu').toggleClass('active');
	      $('.burger-mob').toggleClass('active');
	      $('.header__language-list--mobile').toggleClass('active');


		  $('body').toggleClass('overflowHidden');
		});


	let scrollbarWidth = window.innerWidth - document.documentElement.clientWidth;
	$('body').append('<style>body.overflowHidden { padding-right: '+ scrollbarWidth +'px;}</style>');
  	
});