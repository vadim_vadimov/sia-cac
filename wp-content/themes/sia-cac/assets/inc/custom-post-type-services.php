<?php

// Register Custom Post Type
function custom_post_type_services() {

	$labels = array(
		'name'                  => 'Services',
		'singular_name'         => 'Services',
		'menu_name'             => 'Services',
		'add_new_item'          => 'Add New',
		'add_new'               => 'Add New',
		'new_item'              => 'New',
		'edit_item'             => 'Edit',
		'update_item'           => 'Update',
		'view_item'             => 'View',
		'view_items'            => 'View All',
	);
	$rewrite = array(
		'slug'                  => 'services',
		'with_front'            => true,
		'pages'                 => false,
		'feeds'                 => false,
	);
	$args = array(
		'label'                 => 'News',
		'description'           => 'Sia Cac services',
		'labels'                => $labels,
		'supports'              => array(
			'title',
			'editor'
		),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 6,
		'menu_icon'             => 'dashicons-portfolio',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
		'show_in_rest'          => true
	);
	register_post_type( 'services', $args );

}
add_action( 'init', 'custom_post_type_services', 0 );


?>