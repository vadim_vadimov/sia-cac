<?php 
/**
 *	Template name: About Page 
 */

get_header(); ?>

    <div class="single-page  wow fadeIn">
        <div class="container">
            <div class="single-page__content">
                <div class="single-page__left-col">
                    
                    <?php if (get_field('about_left_texts')) { ?>
                        <?php the_field('about_left_texts') ?>
                    <?php } ?> 

                </div>
                <div class="single-page__right-col">

                    <?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>

                        <h1><?php esc_html( the_title() ) ?></h1>

                    <?php endwhile; ?>
                    <?php endif; ?> 
  
                    <?php if (get_field('about_img')) { ?>
                        <div class="single-page__img-wrap">
                            <div class="single-page__img" style="background-image: url(<?php the_field('about_img') ?>);"></div>
                        </div>
                    <?php } ?>  
                    
                    <?php if (get_field('about_right_texts')) { ?>
                        <?php the_field('about_right_texts') ?>
                    <?php } ?> 
                </div>
                <div class="single-page__icon">
                    <img src="img/nav-1-b.svg" alt="img">
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>