<?php 
/**
 *	Template name: Home Page 
 */

get_header(); ?>

    <section class="intro">
        <div class="container">
            <div class="intro__content">
                <div class="intro__title-content wow fadeInLeft">

                    <?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>

                        <?php esc_html( the_content() ) ?>

                    <?php endwhile; ?>
                    <?php endif; ?> 
                    
                    <div class="divider"></div>

                    <?php if (get_field('sub_title_text')) { ?>
                        <?php the_field('sub_title_text') ?>
                    <?php } ?>  
                    
                    <a href="#info" class="btn-main  scrollto">VIEW MORE</a>

                    <?php if (get_field('intro_text')) { ?>
                        <span class="description-info"><?php the_field('intro_text') ?></span>
                    <?php } ?>  
                    
                </div>
                
                <?php $args = array('post_type' => 'services',
                                    'order' => 'DESC') ?>

                <?php $page_index = new WP_Query($args) ?>

                <div class="intro__nav-list wow fadeInRight" >

                <?php if ($page_index->have_posts() ) :  while ( $page_index->have_posts() ) : $page_index->the_post();?>

                    <a href="<?php echo esc_url( get_permalink() ); ?>" class="intro__nav-item">

                        <?php if (get_field('services_image_homepage_on_hover')) { ?>
                            <div class="intro__nav-hover-img" style="background-image: url(<?php the_field('services_image_homepage_on_hover') ?>)"></div>
                        <?php } ?>

                        <?php if (get_field('service_icon_homepage_small')) { ?>
                            <div class="intro__nav-img">
                                <img src="<?php the_field('service_icon_homepage_small') ?>" alt="img">
                            </div>
                        <?php } ?>

                        <p><?php esc_html( the_title() ) ?></p>
                    </a>
                    
                    <?php endwhile; ?>

                <?php endif; ?> 

                </div>
                
                <?php wp_reset_postdata(); ?>  

            </div>
        </div>
    </section>

    <section class="info"  id="info">
        <div class="container">

            <?php if (get_field('info_title')) { ?>
                <h2 class="wow fadeIn"><?php the_field('info_title') ?></h2>
            <?php } ?>  

            <div class="info__content">

                <?php if (get_field('info_img')) { ?>
                    <div class="info__title-content  wow fadeIn" data-wow-delay="0.3s">
                        <div class="info__title-img">
                            <img src="<?php the_field('info_img') ?>" alt="img">
                        </div>
                    </div>
                <?php } ?>  

                <div class="info__about-content  wow fadeIn" data-wow-delay="0.6s">

                    <?php if (get_field('info_text')) { ?>
                        <?php the_field('info_text') ?>
                    <?php } ?> 

                    <?php if (get_field('info_description')) { ?>
                        <span class="description-info"><?php the_field('info_description') ?></span>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>

    <section class="reviews">
        <div class="container">
            
            <?php if (get_field('review_title')) { ?>
                <h2 class="wow fadeIn"><?php the_field('review_title') ?></h2>
            <?php } ?>  

            <?php if( have_rows('slider_list') ): ?>   
                <div class="reviews__slider wow fadeIn" data-wow-delay="0.3s">
                    <?php while( have_rows('slider_list') ): the_row(); 
                        $img = get_sub_field('slide_img');
                        $text = get_sub_field('slide_text');
                        $name = get_sub_field('slide_name');
                        $position = get_sub_field('slide_position');

                        ?>
                        <div class="reviews__slide">
                            <div class="reviews__slide-item">
                                <div class="reviews__slide-img">
                                    <img src="<?php echo $img; ?>" alt="img">
                                </div>
                                <?php echo $text; ?>
                                <strong class="reviews__slide-name"><?php echo $name; ?></strong>
                                <span class="reviews__slide-position"><?php echo $position; ?></span>
                            </div>
                        </div>

                    <?php endwhile; ?>  
                </div>
            <?php endif; ?> 
            
            <div class="reviews__slider-btns wow fadeIn" data-wow-delay="0.3s">
                <button class="reviews__slider-btn  reviews__slider-btn--prev">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/sl-l.png" alt="prev">
                </button>
                <button class="reviews__slider-btn  reviews__slider-btn--next">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/sl-r.png" alt="next">
                </button>
            </div>
            <div class="reviews__img-content wow fadeIn" data-wow-delay="0.3s">

                <?php if (get_field('foto_img-1')) { ?>
                    <div class="reviews__img-mask">
                        <img src="<?php the_field('foto_img-1') ?>" alt="img">
                    </div>
                <?php } ?>

                <?php if (get_field('foto_img-2')) { ?>
                    <div class="reviews__img-mask">
                        <img src="<?php the_field('foto_img-2') ?>" alt="img">
                    </div>
                <?php } ?>

                <?php if (get_field('foto_img-3')) { ?>
                    <div class="reviews__img-mask">
                        <img src="<?php the_field('foto_img-3') ?>" alt="img">
                    </div>
                <?php } ?>

            </div>
            <div class="reviews__img-brands wow fadeIn" data-wow-delay="0.3s">

                <?php if (get_field('foto_brand-1')) { ?>
                    <div class="reviews__brand-mask">
                        <img src="<?php the_field('foto_brand-1') ?>" alt="img">
                    </div>
                <?php } ?>

                <?php if (get_field('foto_brand-2')) { ?>
                    <div class="reviews__brand-mask">
                        <img src="<?php the_field('foto_brand-2') ?>" alt="img">
                    </div>
                <?php } ?>

                <?php if (get_field('foto_brand-3')) { ?>
                    <div class="reviews__brand-mask">
                        <img src="<?php the_field('foto_brand-3') ?>" alt="img">
                    </div>
                <?php } ?>
            
            </div>
        </div>
    </section>
    
    <section class="contact-info">
        <div class="container">
            <div class="contact-info__tabs  tabs-info-js">
                <div class="contact-info__tabs-list  tabs-js wow fadeIn" data-wow-delay="0.3s">
                    <h2 class="contact-info__tabs-item  active"><?php the_field('contacts_title', 104) ?></li>
                    <h2 class="contact-info__tabs-item"><?php the_field('send_message_title') ?></li>
                </div>
                <div class="contact-info__content">
                    <div class="contact-info__tab-list-wrap  tabs-list-js  active  wow fadeIn">

                        <?php if( have_rows('contacts_list', 104) ): ?>   
                            <div class="contact-info__contacts-list">
                                <?php while( have_rows('contacts_list', 104) ): the_row(); 
                                    $img = get_sub_field('contacts_flag', 104);
                                    $country = get_sub_field('contacts_country', 104);
                                    $text = get_sub_field('contacts_text', 104);

                                    ?>

                                    <div class="contact-info__contact-item">
                                        <div class="contact-info__flag-content">
                                            <div class="contact-info__flag">
                                                <img src="<?php echo $img; ?>" alt="img">
                                            </div>
                                            <h4 class="contact-info__country"><?php echo $country; ?></h4>
                                        </div>
                                        <?php echo $text; ?>
                                    </div>

                                <?php endwhile; ?>  
                            </div>
                        <?php endif; ?> 

                    </div>
                    <div class="contact-info__tab-list-wrap  tabs-list-js">
                        
                        <?php if (get_field('contact_form', 'options')) { ?>
                            <?php the_field('contact_form', 'options') ?>
                        <?php } ?>  

                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>