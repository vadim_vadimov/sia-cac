<?php 
/**
 *	Template name: Contacts Page 
 */

get_header(); ?>

    <section class="contact-info  contact-info--inner  wow fadeIn">
        <div class="container">

            <?php if (get_field('contacts_title')) { ?>
                <h2><?php the_field('contacts_title') ?></h2>
            <?php } ?>

            <?php if( have_rows('contacts_list') ): ?>   
                <div class="contact-info__contacts-list">
                    <?php while( have_rows('contacts_list') ): the_row(); 
                        $img = get_sub_field('contacts_flag');
                        $country = get_sub_field('contacts_country');
                        $text = get_sub_field('contacts_text');

                        ?>

                        <div class="contact-info__contact-item">
                            <div class="contact-info__flag-content">
                                <div class="contact-info__flag">
                                    <img src="<?php echo $img; ?>" alt="img">
                                </div>
                                <h4 class="contact-info__country"><?php echo $country; ?></h4>
                            </div>
                            <?php echo $text; ?>
                        </div>

                    <?php endwhile; ?>  
                </div>
            <?php endif; ?> 
        </div>
    </section>

<?php get_footer(); ?>