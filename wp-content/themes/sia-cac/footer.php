	<footer class="footer wow fadeIn">
		<div class="container">
			<p class="footer__copyrights">All rights reserved</p>
			<div class="footer__designer">
				<p>Made by <a target="_blank" href="https://case-digital.com/">CASE Digital Studio</a></p>
			</div>
		</div>
	</footer>


	<?php wp_footer(); ?>

</body>
</html>