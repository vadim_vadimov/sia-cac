<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="format-detection" content="telephone=no">

    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no" />
    <meta name="HandheldFriendly" content="true" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri() ?>/assets/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri() ?>/assets/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri() ?>/assets/img/favicon/favicon-16x16.png">
    <meta name="theme-color" content="#348681">

    <title><?php wp_title(); ?></title>

    <?php wp_head(); ?>
</head>
<body>

    <header class="header wow fadeIn">
        <div class="container">

            <?php if (get_field('logotype', 'options')) { ?>
                <a href="<?php echo esc_url( home_url() ); ?>" class="header__logo">
                    <img src="<?php the_field('logotype', 'options') ?>" alt="logo">
                    <span><?php the_field('logotype_text', 'options') ?></span>
                </a>
            <?php } ?>  

            <div class="header__info-panel">
                <div class="header__language-list">
                    <?php do_action('wpml_add_language_selector'); ?>
<!--                    <a href="#" class="header__language-item active">EN</a>-->
<!--                    <a href="#" class="header__language-item">LV</a>-->
<!--                    <a href="#" class="header__language-item">RU</a>-->
                </div>
                <div class="burger-content">
                    <div class="burger-mob"></div>
                </div>
                <nav class="header__menu">
                    <div class="header__language-list  header__language-list--mobile">
	                    <?php do_action('wpml_add_language_selector'); ?>
<!--                        <a href="#" class="header__language-item active">EN</a>-->
<!--                        <a href="#" class="header__language-item">LV</a>-->
<!--                        <a href="#" class="header__language-item">RU</a>-->
                    </div>

                    <?php $args = array('post_type' => 'services',
                                        'order' => 'DESC') ?>

                    <?php $page_index = new WP_Query($args) ?>

                    <ul class="header__menu-list">

                    <?php if ($page_index->have_posts() ) :  while ( $page_index->have_posts() ) : $page_index->the_post();?>

                        <li>
                            <a href="<?php echo esc_url( get_permalink() ); ?>">

                                <?php if (get_field('service_icon_homepage_small')) { ?>
                                    <div class="header__menu-item-img">
                                        <img src="<?php the_field('service_icon_homepage_small') ?>" alt="img">
                                    </div>
                                <?php } ?>
                                
                                <span><?php esc_html( the_title() ) ?></span>
                            </a>
                        </li>
                        
                        <?php endwhile; ?>

                    <?php endif; ?> 

                    </ul>
                    
                    <?php wp_reset_postdata(); ?>  

                    <div class="header__menu-info-content">

                        <?php wp_nav_menu(array(
                            'theme_location'  => 'header_menu',
                            'container'       => null, 
                            'menu_class'      => 'header__sub-menu-list', 
                        )); ?>

                        <div class="divider"></div>

                        <?php if (get_field('burger_address', 'options')) { ?>
                            <div class="header__address-info">
                                <?php the_field('burger_address', 'options') ?>
                            </div>
                        <?php } ?>

                    </div>
                </nav>
            </div>
        </div>
    </header>
    