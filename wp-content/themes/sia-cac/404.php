<?php get_header(); ?>
	
	<div class="page-zero">
		<div class="container">
			<span class="page-zero__title">404</span>

			<?php if (get_field('404_text','options')) { ?>
				<p class="page-zero__text"><?php the_field('404_text','options') ?></p>
			<?php } ?>  

		</div>
	</div>
	
<?php get_footer(); ?>