<?php get_header(); ?>

	<div class="single-page  wow fadeIn">
		<div class="container">
			<div class="single-page__content">
				<div class="single-page__left-col">

					<?php if (get_field('service_left_text')) { ?>
					    <?php the_field('service_left_text') ?>
					<?php } ?> 

				</div>
				<div class="single-page__right-col">

					<?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>

					    <h1><?php esc_html( the_title() ) ?></h1>

					<?php endwhile; ?>
					<?php endif; ?> 

					<?php if (get_field('service_main_img')) { ?>
						<div class="single-page__img-wrap">
							<div class="single-page__img" style="background-image: url(<?php the_field('service_main_img') ?>);"></div>
						</div>
					<?php } ?>  

					<?php if (get_field('service_right_text')) { ?>
					    <?php the_field('service_right_text') ?>
					<?php } ?> 

				</div>

				<?php if (get_field('services_icon')) { ?>
					<div class="single-page__icon">
						<img src="<?php the_field('services_icon') ?>" alt="img">
					</div>
				<?php } ?> 
				
			</div>
		</div>
	</div>

<?php get_footer(); ?>