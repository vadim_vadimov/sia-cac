$(function () {

	if($('.reviews__slider').length > 0) {
	    $('.reviews__slider').slick({
	        slidesToShow: 1,
	        infinite: true,
	        dots: false,
	        arrows: false,
	        speed: 500,
	        fade: true,
	        cssEase: 'linear',
	        slidesToScroll: 1,
	    });
	}

	$(".reviews__slider-btn--prev").on("click", function() {
	    $(".reviews__slider").slick("slickPrev");
	});

	$(".reviews__slider-btn--next").on("click", function() {
	    $(".reviews__slider").slick("slickNext");
	});  



	$(".tabs-js").on("click", "h2:not(.active)", function(e) {
	        $(this)
	        .addClass("active")
	        .siblings()
	        .removeClass("active")
	        .parents(".tabs-info-js")
	        .find(".contact-info__content")
	        .children()
	        .hide()
	        .eq($(this).index())
	        .fadeIn(300);

	});

	$('.burger-content').on('click', function () {
		  $('.header__menu').toggleClass('active');
	      $('.burger-mob').toggleClass('active');
	      $('.header__language-list--mobile').toggleClass('active');


		  $('body').toggleClass('overflowHidden');
		});


	let scrollbarWidth = window.innerWidth - document.documentElement.clientWidth;
	$('body').append('<style>body.overflowHidden { padding-right: '+ scrollbarWidth +'px;}</style>');
  	

  	function pageWidget(pages) {
  	        var widgetWrap = $('<div class="widget_wrap"><ul class="widget_list"></ul></div>');
  	        widgetWrap.prependTo("body");
  	        for (var i = 0; i < pages.length; i++) {
  	            $('<li class="widget_item"><a class="widget_link" href="' + pages[i][0] + '.html' + '">' + pages[i][1] + '</a></li>').appendTo('.widget_list');
  	        }
  	        var widgetStilization = $('<style>body {position:relative} .widget_wrap{position:absolute;top:0;left:0;z-index:9999;padding:5px 10px;background:#222;border-bottom-right-radius:10px;-webkit-transition:all .3s ease;transition:all .3s ease;-webkit-transform:translate(-100%,0);-ms-transform:translate(-100%,0);transform:translate(-100%,0)}.widget_wrap:after{content:" ";position:absolute;top:0;left:100%;width:24px;height:24px;background:#222 url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAgMAAABinRfyAAAABGdBTUEAALGPC/xhBQAAAAxQTFRF////////AAAA////BQBkwgAAAAN0Uk5TxMMAjAd+zwAAACNJREFUCNdjqP///y/DfyBg+LVq1Xoo8W8/CkFYAmwA0Kg/AFcANT5fe7l4AAAAAElFTkSuQmCC) no-repeat 50% 50%;cursor:pointer}.widget_wrap:hover{-webkit-transform:translate(0,0);-ms-transform:translate(0,0);transform:translate(0,0)}.widget_list{padding: 0;}.widget_item{list-style:none;padding:0 0 10px}.widget_link{color:#fff;text-decoration:none;font-size:15px;}.widget_link:hover{text-decoration:underline} </style>');
  	        widgetStilization.prependTo(".widget_wrap");
  	    }

  	    pageWidget([
  	        ['index', 'Главная'],
  	        ['single', 'Сингл'],
  	        ['about', 'О нас'],
  	        ['contacts', 'Контакты']
  	    ]);
});